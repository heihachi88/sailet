<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

        <title>Sailet form</title>

        {{-- css --}}
        <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
    </head>

    <body class="pdf">
        <div class="container">
            <div class="row">
                <div class="col-md-8 offset-md-2">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">Наименование поля</th>
                                <th scope="col">Значение</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td>ФИО</td>
                                <td>{{ $fio }}</td>
                            </tr>                            
                            <tr>
                                <td>ИИН</td>
                                <td>{{ $iin }}</td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td>{{ $email }}</td>
                            </tr>
                            <tr>
                                <td>Телефон</td>
                                <td>{{ $phone }}</td>
                            </tr>
                            <tr>
                                <td>Адрес</td>
                                <td>{{ $address }}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>
