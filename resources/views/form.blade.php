<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

        <title>Sailet form</title>

        {{-- css --}}
        <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
    </head>

    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-8 offset-md-2 mt-6">
                    <h1 class="mb-5">Sailet PDF Form</h1>

                    <form action="/pdf" method="POST">
                        @csrf
                        <div class="form-group row">
                            <label for="fio" class="col-md-3 col-form-label">ФИО</label>
                            <div class="col-md-9">
                                <input type="text" name="fio" class="form-control {{ $errors->has('fio') ? 'is-invalid' : '' }}" id="fio" value="{{ old('fio') }}">
                                @if ($errors->has('fio'))
                                    <small id="fio" class="form-text text-muted">{{ $errors->first('fio') }}</small>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="iin" class="col-md-3 col-form-label">ИИН</label>
                            <div class="col-md-9">
                                <input type="text" name="iin" class="form-control {{ $errors->has('iin') ? 'is-invalid' : '' }}" id="iin" value="{{ old('iin') }}">
                                @if ($errors->has('iin'))
                                    <small id="iin" class="form-text text-muted">{{ $errors->first('iin') }}</small>
                                @endif
                            </div>
                        </div>                        

                        <div class="form-group row">
                            <label for="email" class="col-md-3 col-form-label">Email</label>
                            <div class="col-md-9">
                                <input type="email" name="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" id="email" value="{{ old('email') }}">
                                @if ($errors->has('email'))
                                    <small id="email" class="form-text text-muted">{{ $errors->first('email') }}</small>
                                @endif
                            </div>
                        </div>                        

                        <div class="form-group row">
                            <label for="phone" class="col-md-3 col-form-label">Номер телефона</label>
                            <div class="col-md-9">
                                <input type="text" name="phone" class="form-control {{ $errors->has('phone') ? 'is-invalid' : '' }}" id="phone" value="{{ old('phone') }}">
                                @if ($errors->has('phone'))
                                    <small id="phone" class="form-text text-muted">{{ $errors->first('phone') }}</small>
                                @endif
                            </div>
                        </div>                        

                        <div class="form-group row">
                            <label for="address" class="col-md-3 col-form-label">Адрес</label>
                            <div class="col-md-9">
                                <input type="text" name="address" class="form-control {{ $errors->has('address') ? 'is-invalid' : '' }}" id="address" value="{{ old('address') }}">
                                @if ($errors->has('address'))
                                    <small id="address" class="form-text text-muted">{{ $errors->first('address') }}</small>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12 text-center mt-3">
                                <button type="submit" class="btn btn-success">Сгенерировать PDF</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        {{-- js scripts --}}
        <script src="{{ mix('/js/app.js') }}"></script>
        <script src="/js/jquery.inputmask.bundle.js"></script>
        <script src="https://unpkg.com/sweetalert2@7.18.0/dist/sweetalert2.all.js"></script>
        @include('sweetalert::alert')
    </body>
</html>
