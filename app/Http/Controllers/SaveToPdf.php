<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use PDF;

class SaveToPdf extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fio' => 'required|max:40',
            'iin' => 'required|digits:12',
            'email' => 'required|email',
            'phone' => 'required|digits:9',
            'address' => 'required|min:15|max:100',
        ]);

        if ($validator->fails()) {
            // alert()->error('Ошибка', $validator->errors()->first());
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $data = [
            "fio" => $request->input('fio'),
            "iin" => $request->input('iin'),
            "email" => $request->input('email'),
            "phone" => $request->input('phone'),
            "address" => $request->input('address'),
        ];

        $pdf = PDF::loadView('summary', $data);
        return $pdf->download('summary.pdf');
    }
}
