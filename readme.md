## Steps to install the project

1. Clone repository
2. `composer install`
3. Create `.env` file and paste `APP_KEY` - `php artisan key:generate`
4. Done, navigate to the front page.